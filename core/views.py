from django.shortcuts import render
from django.http import HttpResponse
import requests

# Create your views here.
def index(request):
    payload = {'order': 'updown'}
    responses = requests.get('https://backend-calis.herokuapp.com/feeds', params=payload)
    # print(responses.status_code)
    # print(responses.json()[0])
    # list_ = []
    # for instance_ in responses.json():
    #     print(instance_)
    #     print(instance_.get("id"))

    return render(request, 'core/index.html', {'datas':responses.json()})